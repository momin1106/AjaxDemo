package org.momin.service;

import java.util.List;

import org.momin.bean.Roles;
import org.momin.bean.UserInfo;


public interface IUserService {
	/**根据用户名查询数据*/
	boolean getUserByName(String name);
	
	/**根据用户名和密码查询用户信息*/
	UserInfo getUserByNameAndPwd(String name,String pwd);
	
	/**分页*/
	List<UserInfo> getUserPage(int pageIndex,int pageSize);
	
	boolean deleteUser(int id);
	
	/*修改用户*/
	int updateUser(List<Object> list,int id);
	
	UserInfo getUserById(int id);
	
	List<Roles> getRoles();
}
