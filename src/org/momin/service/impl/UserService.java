package org.momin.service.impl;

import java.util.List;

import org.momin.bean.Roles;
import org.momin.bean.UserInfo;
import org.momin.dao.IUserDao;
import org.momin.dao.impl.UserDao;
import org.momin.service.IUserService;

public class UserService implements IUserService {

	IUserDao dao=new UserDao();
	@Override
	public boolean getUserByName(String name) {
		return dao.getUserByName(name);
	}

	@Override
	public UserInfo getUserByNameAndPwd(String name, String pwd) {
		return dao.getUserByNameAndPwd(name, pwd);
	}

	@Override
	public List<UserInfo> getUserPage(int pageIndex, int pageSize) {
		return dao.getUserPage(pageIndex, pageSize);
	}

	@Override
	public boolean deleteUser(int id) {
		return dao.deteleUser(id)>0;
	}

	@Override
	public int updateUser(List<Object> list,int id) {
		return dao.updateUser(list,id);
	}

	@Override
	public UserInfo getUserById(int id) {
		return dao.getUserById(id);
	}

	@Override
	public List<Roles> getRoles() {
		return dao.getRoles();
	}

}
