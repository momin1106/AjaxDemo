package org.momin.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.momin.bean.Roles;
import org.momin.bean.UserInfo;
import org.momin.dao.IUserDao;
import org.momin.tools.DBTools;

public class UserDao implements IUserDao {

	@Override
	/*根据用户名查询用户*/
	public boolean getUserByName(String name) {
		String sql="select * from userInfo where name=?";
		List<Object> list=new ArrayList<Object>();
		list.add(name);
		ResultSet rs=DBTools.getReusultSet(sql,list);
		try {
			if(rs.next()){
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			DBTools.close();
		}
		return false;
	}

	@Override
	public UserInfo getUserByNameAndPwd(String name, String pwd) {
		String sql="select userinfo.id as userinfo_id ,userinfo.name as userinfo_name,userinfo.pwd as userinfo_pwd,userinfo.rolesid as userinfo_rolesid ,roles.id as roles_id,roles.name as roles_name from userinfo,roles where userinfo.name=? and userinfo.pwd=? and userinfo.rolesid=roles.id";
		List<Object> list=new ArrayList<Object>();
		list.add(name);
		list.add(pwd);
		return DBTools.getObject(UserInfo.class, sql, list);
		
	}

	@Override
	public List<UserInfo> getUserPage(int pageSize, int pageIndex) {
		String sql="select * from (select rownum as r_id,userinfo.id as userinfo_id,userinfo.name as userinfo_name,userinfo.pwd as userinfo_pwd,userinfo.rolesid as userinfo_rolesid,roles.id as roles_id,roles.name as roles_name from userinfo,roles where userinfo.rolesid=roles.id)where r_id>? and r_id<?";
		List<Object> list=new ArrayList<Object>();
		//System.out.println("index:"+(pageIndex-1)*pageSize);
		//System.out.println("index:"+pageIndex*pageSize+1);
		list.add((pageIndex-1)*pageSize);
		list.add(pageIndex*pageSize+1);
		return DBTools.getListBySql(UserInfo.class, sql, list);
	}

	@Override
	public int deteleUser(int id) {
		return DBTools.deleteObject(UserInfo.class, id);
	}

	@Override
	public int updateUser(List<Object> list,int id) {
		String sql="update userinfo set name=?,pwd=? , rolesid=? where id="+id;
		//System.out.println(sql);
		return DBTools.executeUpdate(sql, list);
	}

	@Override
	public UserInfo getUserById(int id) {
		return DBTools.getObject(UserInfo.class, id);
	}

	@Override
	public List<Roles> getRoles() {
		String sql="select id,name from roles";
		List<Object> list=new ArrayList<Object>();
		return DBTools.getListBySql(Roles.class,sql,list);
	}
	

}
