package org.momin.bean;

import org.momin.bean.Roles;

public class UserInfo {
	private int id;
	private String name;
	private String pwd;
	private int rolesid;
	private Roles rinfo;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPwd() {
		return pwd;
	}
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}
	public int getRolesid() {
		return rolesid;
	}
	public void setRolesid(int rolesid) {
		this.rolesid = rolesid;
	}
	public Roles getRinfo() {
		return rinfo;
	}
	public void setRinfo(Roles rinfo) {
		this.rinfo = rinfo;
	}
	@Override
	public String toString() {
		return "UserInfo [id=" + id + ", name=" + name + ", pwd=" + pwd
				+ ", rolesid=" + rolesid + ", rinfo=" + rinfo + "]";
	}
	public UserInfo(int id, String name, String pwd, int rolesid, Roles rinfo) {
		super();
		this.id = id;
		this.name = name;
		this.pwd = pwd;
		this.rolesid = rolesid;
		this.rinfo = rinfo;
	}
	public UserInfo() {
		super();
	}
}
