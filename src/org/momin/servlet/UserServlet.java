package org.momin.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.momin.bean.Roles;
import org.momin.bean.UserInfo;
import org.momin.service.IUserService;
import org.momin.service.impl.UserService;

import com.google.gson.Gson;


@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    IUserService service=new UserService();   
    Gson gson=new Gson();
    public UserServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String type=request.getParameter("type");
		try {
			Method m=UserServlet.class.getMethod(type,HttpServletRequest.class,HttpServletResponse.class);
			m.invoke(this,request,response);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	/*登陆*/
	public void login(HttpServletRequest request,HttpServletResponse response){
		//System.out.println("login");
		String name=request.getParameter("name");
		String pwd=request.getParameter("pwd");
		UserInfo info=service.getUserByNameAndPwd(name, pwd);
		try {
			if(info!=null){
				request.getSession().setAttribute("info", info);
				//用于文件的上传与下载，转换为二进制
				//response.getOutputStream().print("1");
				//输出字符串专用
				response.getWriter().print("1");
			}else{
				response.getWriter().print("0");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*分页*/
	public void getUserPage(HttpServletRequest request,HttpServletResponse response) throws IOException{
		int pageIndex=request.getParameter("pageIndex")==null?1:Integer.parseInt(request.getParameter("pageIndex"));
		int pageSize=request.getParameter("pageSize")==null?10:Integer.parseInt(request.getParameter("pageIndex"));
		
		List<UserInfo> list=service.getUserPage(pageIndex, pageSize);
		
		if(list!=null&&list.size()>0){
			String msg=gson.toJson(list);
			//System.out.println(msg);
			response.getWriter().print(msg);
		}else{
			response.getWriter().print("0");
		}
		response.getWriter().flush();
	}
	public void deleteUser(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		int id=request.getParameter("id")==null?0:Integer.parseInt(request.getParameter("id").toString());
		if (service.deleteUser(id))
			response.getWriter().print("1");
		else
			response.getWriter().print("0");
		response.getWriter().close();
	}
	
	public void getUserByName(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException, InterruptedException{
		Thread.sleep(2000);
		String name=request.getParameter("name");
		if (service.getUserByName(name))
			response.getWriter().print("1");
		else
			response.getWriter().print("0");
	}
	
	public void updateUser(HttpServletRequest request,HttpServletResponse response) throws IOException{
		int id=Integer.parseInt(request.getParameter("id").toString());
		List<Object> list=new ArrayList<Object>();
		list.add(request.getParameter("name"));
		list.add(request.getParameter("pwd"));
		list.add(request.getParameter("rolesid"));
		if(service.updateUser(list, id)>0){
			response.getWriter().print("1");
		}else{
			response.getWriter().print("0");
		}
	}
	public void getUserById(HttpServletRequest request,HttpServletResponse response) throws IOException{
		int id=Integer.parseInt(request.getParameter("id").toString());
		UserInfo info=service.getUserById(id);
		String msg=gson.toJson(info);
		System.out.println(msg);
		response.getWriter().print(msg);
	}
	
	public void getRoles(HttpServletRequest request,HttpServletResponse response) throws IOException{
		List<Roles> list=service.getRoles();
		if(list!=null&&list.size()>0){
			String msg=gson.toJson(list);
			System.out.println(msg);
			response.getWriter().print(msg);
		}else{
			response.getWriter().print("0");
		}
	}
	
	
	
}
